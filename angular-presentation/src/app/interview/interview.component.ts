import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { InterviewModel, PostInterviewResponceModel } from './model/interview.model';
import { InterviewService } from './servieces/interview.service';
import { InterviewsFacade } from './store/interview.facade';
import { RecruiterModel } from 'src/app/models/recruiter.model'
import { MatDialog } from '@angular/material/dialog';
import { InterviewCreateComponent } from './interview-create/interview-create.component';
import { InterviewEditComponent } from './interview-edit/interview-edit.component';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InterviewComponent implements OnInit {

  constructor(public service: InterviewService, public facade: InterviewsFacade, public matDialog: MatDialog) { }

  public interviewList$: Observable<Array<InterviewModel>>;


  ngOnInit(): void {
    this.facade.dispatchInterviews();
    this.interviewList$ = this.facade.getInterviews();
  }

  public displayedColumns: string[] = ['title', 'recruiter', 'date', 'candidate', 'salary', 'btns'];

  public onSumbit(): void {
    this.matDialog.open(InterviewCreateComponent);
  }

  public onDelete(id: number): void {
    this.facade.deleteInterview(id);
  }

  public onEdit(interview: InterviewModel): void {
    this.matDialog.open(InterviewEditComponent, { data: interview })
  }
}
