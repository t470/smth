import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { GetInterviewResponseModel, InterviewModel, PostInterviewResponceModel } from "../model/interview.model";

@Injectable({
    providedIn: 'root'
})

export class InterviewService {
    public options = {
        headers: new HttpHeaders({
            'accept': 'text/plain',
            'api-version': '1.0',
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlFqRkJSRGMxUTBSRU9UTTROelpHUXpFelJqRTROa0k0UlVJM1F6WTJNa1pCUlRZMk9EVTRNUSJ9.eyJodHRwczovL2Rldm94c29mdHdhcmUuY29tL3JvbGVzIjpbXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjoib2xla3NhbmRyLm1vcm96b3ZAZGV2b3hzb2Z0d2FyZS5jb20iLCJpc3MiOiJodHRwczovL2Rldm94c29mdHdhcmUuZXUuYXV0aDAuY29tLyIsInN1YiI6IndhYWR8Z0hQV0pqY3RUNWE3NUc3TzhENTZmMmtuWTZZM1BDT2Q1NWdqSnRnc3VzTSIsImF1ZCI6Imh0dHBzOi8vb25ib2FyZGluZ3RyYWNrZXIvYXBpIiwiaWF0IjoxNjM1NjAyNzI3LCJleHAiOjE2MzU2MDk5MjcsImF6cCI6ImhnT0pqaDFuNDR3RVUyMDBsdEJ6a3JxMjBJT0FMNlBmIiwic2NvcGUiOiIifQ.wRzyL4LnQHsn1ubYkzJym2YetDpKFIUo2LHXRQvPfcoTzZr4Xx6P9iablBcVex8AwvoaS3heq9ghQCRridaXxy52QF89dcVP1RkQvPeTmu5BwsMSqHm4Bk-UZ5AFd1v91gJYLFcsFkazSK1K8nGnZMdoTmrOXGd6micXbZvfCknJVI0vO9XSYLxJKzdf0nuJwzN9TFKXAOxLmDdnbZTodGY_p9-Cy8McuHzuUgRYaDU58XvRC58oviji4w9-ZAmpVUobbimE2t686plz_M4zFqKbf98Da7Drp7y2BkoVUUvykvBo7A00UKo51c47XgXDXOdSHM9O1POsVn0ljSM__g'
        })

    };


    constructor(private httpClient: HttpClient) { }

    public getEntity(): Observable<Array<InterviewModel>> {

        return this.httpClient.get<GetInterviewResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Interview?pageNumber=1&pageSize=10`, this.options)
            .pipe(map(x => (
                x.items
            )
            ));

    }

    public getEntityById(id: number): Observable<InterviewModel> {
        return this.httpClient.get<InterviewModel>(`http://dev0.devoxsoftware.com:4444/api/Interview/${id}`, this.options);
    }

    public postEntity(card: PostInterviewResponceModel): Observable<InterviewModel> {
        return this.httpClient.post<InterviewModel>(`http://dev0.devoxsoftware.com:4444/api/Interview`, card, this.options);
    }

    public putEntity(card: InterviewModel): Observable<InterviewModel> {

        return this.httpClient.put<InterviewModel>(`http://dev0.devoxsoftware.com:4444/api/Interview/$`, {
            id: card.id,
            title: card.title,
            date: card.date,
            address: card.address,
            candidateId: card.candidate.id,
            recruiterId: card.recruiter.id,
            vacancyId: card.vacancy.id
        }, this.options);

    }

    public deleteEntity(id: number): Observable<GetInterviewResponseModel> {
        return this.httpClient.delete<GetInterviewResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Interview/${id}`, this.options)
    }
}
