import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CandidateService } from 'src/app/candidate/services/candidate.service';
import { RecruiterService } from 'src/app/recruiter/services/recruiter.service';
import { VacancyService } from 'src/app/vacancy/services/vacancy.service';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { InterviewModel, PostInterviewResponceModel } from '../model/interview.model';
import { InterviewService } from '../servieces/interview.service';
import { InterviewsFacade } from '../store/interview.facade';

@Component({
  selector: 'app-interview-edit',
  templateUrl: 'interview-edit.component.html',
  styleUrls: ['interview-edit.component.scss']
})
export class InterviewEditComponent implements OnInit {

  public interview: InterviewModel;

  public editForm = new FormGroup({
    title: new FormControl(this.data.title, [Validators.required, customValidator()]),
    date: new FormControl(this.data.date, Validators.required),
    address: new FormControl(this.data.address, Validators.required),
    candidate: new FormControl(this.data.candidate, Validators.required),
    recruiter: new FormControl(this.data.recruiter, Validators.required),
    vacancy: new FormControl(this.data.vacancy, Validators.required),
  });


  public interviewService = this.service;
  public candidateService$ = this.candidateService.candidatesCards$;
  public recruiterService$ = this.recruiterService.recruitersCards$;
  public vacancyService$ = this.vacancyService.vacancyCards$;

  constructor(
    private service: InterviewService,
    private facade: InterviewsFacade,
    private candidateService: CandidateService,
    private recruiterService: RecruiterService,
    private vacancyService: VacancyService,
    @Inject(MAT_DIALOG_DATA) public data: InterviewModel
  ) { }

  ngOnInit() {

  }

  submit() {
    this.interview ={
      ...this.data,
      ...this.editForm.value
    }

    console.log(this.interview);
    this.facade.editInterview(this.interview);
  }
}


