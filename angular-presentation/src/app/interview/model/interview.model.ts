import { CandidateModel } from "src/app/candidate/models/candidate.model";
import { RecruiterModel } from "src/app/models/recruiter.model";
import { VacancyModel } from "src/app/models/vacancy.model";


export interface GetInterviewResponseModel {
    items: Array<InterviewModel>;
}

export interface InterviewModel {
    id: number;
    title: string;
    date: string;
    address: string;
    candidate: CandidateModel;
    recruiter: RecruiterModel;
    vacancy: VacancyModel
}

export interface PostInterviewResponceModel{
    title: string;
    date: string;
    address: string;
    description: string;
    candidateId: number;
    recruiterId: number;
    vacancyId: number;
}