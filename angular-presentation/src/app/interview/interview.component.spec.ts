import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { InterviewComponent } from './interview.component';
import { InterviewService } from './servieces/interview.service';
import { AppModule } from '../app.module';
import { InterviewModel } from './model/interview.model';
import { Observable, of } from 'rxjs';
import { InterviewModule } from './interview.module';
const INTERVIEWS: InterviewModel[] = [
  {
    id: 1,
    title: 'smth',
    date: 'string',
    address: 'string',
    candidate: null,
    recruiter: null,
    vacancy: null
  },
  {
    id: 2,
    title: 'smth',
    date: 'string',
    address: 'string',
    candidate: null,
    recruiter: null,
    vacancy: null
  }
];

const interviewServiceStub = {
  getInterviews(): Observable<InterviewModel[]> {
    return of(INTERVIEWS);
  }
};

describe('InterviewComponent', () => {
  let component: InterviewComponent;
  let fixture: ComponentFixture<InterviewComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppModule],
      declarations: [InterviewComponent],
      providers: [{ provide: InterviewService, useValue: interviewServiceStub }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('header title should be ', () => {
    console.log(fixture.nativeElement);
    expect(fixture.nativeElement.querySelector('.header__title')).toBeTruthy();
  })

});
