import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterviewComponent } from './interview.component';
import { InterviewRoutingModule } from './interview-routing.module';
import { InterviewsFacade } from './store/interview.facade';
import { StoreModule } from '@ngrx/store';
import { interviewReducer } from './store/interview.reducer';
import { EffectsModule } from '@ngrx/effects';
import { InterviewEffects } from './store/interview.effects';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import { InterviewCreateComponent } from './interview-create/interview-create.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { InterviewEditComponent } from './interview-edit/interview-edit.component';



@NgModule({
  declarations: [
    InterviewComponent,
    InterviewCreateComponent,
    InterviewEditComponent,
  ],
  imports: [
    CommonModule,
    MatTableModule,
    InterviewRoutingModule,
    StoreModule.forFeature('interviews', interviewReducer),
    EffectsModule.forFeature([InterviewEffects]),
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatFormFieldModule,
    MatButtonModule,
    ReactiveFormsModule
  ],
  providers: [
    InterviewEffects,
    InterviewsFacade
  ]
})
export class InterviewModule { }
