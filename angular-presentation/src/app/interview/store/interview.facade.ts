import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { InterviewModel, PostInterviewResponceModel } from '../model/interview.model';
import { InterviewState } from './interview.reducer';

import * as fromInterviewsActions from './interview.actions';
import { interviewsQuery } from './interview.selectors';
import { ThrowStmt } from '@angular/compiler';

@Injectable()
export class InterviewsFacade {

    constructor(private store: Store<InterviewState>) { }

    dispatchInterviews() {
        this.store.dispatch(fromInterviewsActions.loadInterviews());
    }

    dispatchInterview(interview: PostInterviewResponceModel) {
        this.store.dispatch(fromInterviewsActions.addInterview({ interview }));
    }

    getInterviews() {
        return this.store.select(interviewsQuery.getInterviews);
    }

    deleteInterview(id: number) {
        return this.store.dispatch(fromInterviewsActions.deleteInterview({ id }));
    }

    editInterview(interview: InterviewModel) {
        return this.store.dispatch(fromInterviewsActions.editInterview({ interview }));
    }

    getIsInterviewLoading() {
        return this.store.select(interviewsQuery.isInterviewLoading);
    }

    getIsInterviewLoaded() {
        return this.store.select(interviewsQuery.isInterviewLoaded);
    }

    getIsInterviewFailed() {
        return this.store.select(interviewsQuery.isInterviewFailed);
    }
    getIsInterviewAddFailed() {
        return this.store.select(interviewsQuery.isInterviewAddFailed);
    }
}
