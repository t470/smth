import { createSelector, createFeatureSelector } from '@ngrx/store';
import { InterviewModel } from '../model/interview.model';

import * as fromInterviewsReducer from './interview.reducer'


export const getInterviewsSelector = createFeatureSelector<fromInterviewsReducer.InterviewState>('interviews');

export const getInterviews = createSelector(
    getInterviewsSelector,
    (state: fromInterviewsReducer.InterviewState) =>
        state.ids.map((id: string | number) => state.entities[id])
);

export const isInterviewLoading = createSelector(
    getInterviewsSelector,
    (state: fromInterviewsReducer.InterviewState) => state.loading
);

export const isInterviewLoaded = createSelector(
    getInterviewsSelector,
    (state: fromInterviewsReducer.InterviewState) => state.loaded
);

export const isInterviewFailed = createSelector(
    getInterviewsSelector,
    (state: fromInterviewsReducer.InterviewState) => state.error
);

export const isInterviewAddFailed = createSelector(
    getInterviewsSelector,
    (state: fromInterviewsReducer.InterviewState) => state.error
);


export const interviewsQuery = {
    isInterviewAddFailed,
    getInterviews,
    isInterviewLoading,
    isInterviewLoaded,
    isInterviewFailed
};