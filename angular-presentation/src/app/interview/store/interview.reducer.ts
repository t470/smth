import { act } from "@ngrx/effects";
import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Action, createReducer, on } from "@ngrx/store";
import { InterviewModel } from "../model/interview.model";

import * as fromInterviewActions from './interview.actions'

export interface InterviewState extends EntityState<InterviewModel> {
    loaded: boolean;
    loading: boolean;
    error: Error
}

const interviewAdapter: EntityAdapter<InterviewModel> = createEntityAdapter<InterviewModel>({
    selectId: (interview: InterviewModel) => interview.id
});

export const interviewInitialState: InterviewState = interviewAdapter.getInitialState({
    loaded: false,
    loading: false,
    error: null
});

const reducer = createReducer(
    interviewInitialState,
    on(fromInterviewActions.loadInterviews, (state, _) => {
        return {
            ...state,
            loading: true
        };
    }),
    on(fromInterviewActions.loadInterviewsApiSuccess, (state, action) => {
        return interviewAdapter.upsertMany(action.interviews, {
            ...state,
            loaded: true,
            loading: false
        });
    }),
    on(fromInterviewActions.loadInterviewsFail, (state, action) => {
        return interviewAdapter.removeAll({
            ...state,
            loaded: false,
            loading: false,
            error: action.error
        });
    }),

    on(fromInterviewActions.addInterview, (state, action) => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(fromInterviewActions.addInterviewSuccess, (state, action) => {
        return interviewAdapter.addOne(action.interview, {
            ...state,
            loading: false,
            loaded: true
        }
        );
    }),

    on(fromInterviewActions.addInterviewFail, (state, action) => {
        return {
            ...state,
            loaded: false,
            loading: false,
            error: action.error
        }
    }),
    on(fromInterviewActions.deleteInterview, (state, action) => {
        return interviewAdapter.removeOne(action.id, {
            ...state,
            loading: true,
        });
    }),

    on(fromInterviewActions.deleteInterviewSuccess, (state, action) => {
        return {
            ...state,
            loading: false,
            loaded: true,
        };
    }),

    on(fromInterviewActions.deleteInterviewFail, (state, action) => {
        return {
            ...state,
            loading: false,
            loaded: false,
            error: action.error,
        };
    }),
    on(fromInterviewActions.editInterview, (state, action) => {
        return interviewAdapter.upsertOne(action.interview, {
            ...state,
            loading: true,
        });
    }),

    on(fromInterviewActions.editInterviewSuccess, (state, action) => {
        return {
            ...state,
            loading: false,
            loaded: true,
        };
    }),

    on(fromInterviewActions.editInterviewFail, (state, action) => {
        return {
            ...state,
            loading: false,
            loaded: false,
            error: action.error,
        };
    })

);

export function interviewReducer(state: InterviewState = interviewInitialState, action: Action) {
    return reducer(state, action);
}
