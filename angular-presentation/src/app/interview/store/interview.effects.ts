import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, concatMap, map, switchMap, withLatestFrom } from "rxjs/operators";
import { InterviewModel } from "../model/interview.model";
import { InterviewService } from "../servieces/interview.service";
import * as fromInterviewActions from './interview.actions'
import { InterviewsFacade } from "./interview.facade";

@Injectable()
export class InterviewEffects {

    constructor(
        private actions$: Actions,
        private service: InterviewService,
        private facade: InterviewsFacade
    ) { }

    public postInterview: InterviewModel;

    loadInterviews = createEffect(() =>
        this.actions$.pipe(
            ofType(fromInterviewActions.loadInterviews),
            switchMap(() => {
                return this.service.getEntity().pipe(
                    map(result => fromInterviewActions.loadInterviewsApiSuccess({ interviews: result })),
                    catchError(error => of(fromInterviewActions.loadInterviewsFail({ error }))))
            })
        )
    );

    addInterview = createEffect(() =>
        this.actions$.pipe(
            ofType(fromInterviewActions.addInterview),
            switchMap((action) => {
                return this.service.postEntity(action.interview);
            }),
            map(result => {
                return fromInterviewActions.addInterviewSuccess({ interview: result })
            }),
            catchError(error => of(fromInterviewActions.addInterviewFail({ error })))
        ));

    deleteInterview = createEffect(() =>
        this.actions$.pipe(
            ofType(fromInterviewActions.deleteInterview),
            switchMap((action) => {
                return this.service.deleteEntity(action.id);
            }),
            map(result => {
                return fromInterviewActions.deleteInterviewSuccess();
            }),
            catchError(error => of(fromInterviewActions.deleteInterviewFail({ error })))
        ));

    editInterview  = createEffect(() =>
    this.actions$.pipe(
        ofType(fromInterviewActions.editInterview),
        switchMap((action) => {
            return this.service.putEntity(action.interview);
        }),
        map(result => {
            return fromInterviewActions.editInterviewSuccess();
        }),
        catchError(error => of(fromInterviewActions.editInterviewFail({ error })))
    ));

}


