
import { createAction, props } from "@ngrx/store";
import { InterviewModel, PostInterviewResponceModel } from "../model/interview.model";


export const loadInterviews = createAction(
    '[interviews] LOAD_INTERVIEWS'
);

export const loadInterviewsApiSuccess = createAction(
    '[interviews] GET_INTERVIEWS',
    props<{ interviews: Array<InterviewModel> }>()
);

export const loadInterviewsFail = createAction(
    '[interviews] LOAD_INTERVIEWS_FAIL',
    props<{ error: Error }>()
);


export const addInterview = createAction(
    '[interviews] ADD_INTERVIEW',
    props<{ interview: PostInterviewResponceModel }>()
);

export const addInterviewSuccess = createAction(
    '[interview] ADD_INTERVIEW_SUCCESS',
    props<{ interview: InterviewModel }>()
);


export const addInterviewFail = createAction(
    '[interview] ADD_INTERVIEW_FAIL',
    props<{ error: Error }>()
)

export const deleteInterview = createAction(
    '[interview] DELETE_INTERVIEW',
    props<{ id: number }>()
)

export const deleteInterviewSuccess = createAction(
    '[interview] DELETE_INTERVIEW_SUCCESS'
)

export const deleteInterviewFail = createAction(
    '[interview] DELETE_INTERVIEW_FAIL',
    props<{ error: Error }>()
)

export const editInterview = createAction(
    '[interview] EDIT_INTERVIEW',
    props<{ interview: InterviewModel }>()
)

export const editInterviewSuccess = createAction(
    '[interview] EDIT_INTERVIEW_SUCCESS'
)

export const editInterviewFail = createAction(
    '[interview] EDIT_INTERVIEW_FAIL',
    props<{ error: Error }>()
)

