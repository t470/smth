import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CandidateService } from 'src/app/candidate/services/candidate.service';
import { RecruiterService } from 'src/app/recruiter/services/recruiter.service';
import { VacancyService } from 'src/app/vacancy/services/vacancy.service';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { InterviewModel, PostInterviewResponceModel } from '../model/interview.model';
import { InterviewService } from '../servieces/interview.service';
import { InterviewsFacade } from '../store/interview.facade';

@Component({
  selector: 'app-interview-create',
  templateUrl: 'interview-create.component.html',
  styleUrls: ['interview-create.component.scss']
})
export class InterviewCreateComponent {

  public interview: PostInterviewResponceModel;

  public createForm = new FormGroup({
    title: new FormControl('', [Validators.required, customValidator()]),
    date: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    candidateId: new FormControl('', Validators.required),
    recruiterId: new FormControl('', Validators.required),
    vacancyId: new FormControl('', Validators.required),
  });


  public interviewService = this.service;
  public candidateService$ = this.candidateService.candidatesCards$;
  public recruiterService$ = this.recruiterService.recruitersCards$;
  public vacancyService$ = this.vacancyService.vacancyCards$;

  constructor(
    private service: InterviewService,
    private facade: InterviewsFacade,
    private candidateService: CandidateService,
    private recruiterService: RecruiterService,
    private vacancyService: VacancyService
  ) { }

  public submit(): void {
    this.interview = this.createForm.value;
    this.facade.dispatchInterview(this.interview);
  }
}


