import { Component, Input } from '@angular/core';

import { MatDialog } from "@angular/material/dialog";
import { CandidateModel } from "../models/candidate.model";
import { CandidateService } from '../services/candidate.service';
import { CandidateCardEditComponent } from '../card-candidate-edit/card-candidate-edit.component';

@Component({
  selector: 'app-card-candidate',
  templateUrl: './card-candidate.component.html',
  styleUrls: ['./card-candidate.component.scss']
})
export class CardCandidateComponent {

  @Input() card: CandidateModel;

  constructor(private matDialog: MatDialog, private service: CandidateService) {
  }

  public onOpenDialogClick(): void {
    this.matDialog.open(CandidateCardEditComponent, {
      data: this.card,
    });
  }

  public delete(): void {
    this.service.currentCandidate = this.card;
    this.service.deleteCandidate();
  }
}
