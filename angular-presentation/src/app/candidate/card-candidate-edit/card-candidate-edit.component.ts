import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { CandidateModel, FullCandidateModel } from '../models/candidate.model';
import { CandidateService } from "../services/candidate.service";

@Component({
  selector: 'app-card-candidate-edit',
  templateUrl: 'card-candidate-edit.component.html',
  styleUrls: ['card-candidate-edit.component.scss']
})
export class CandidateCardEditComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: CandidateModel,
    private service: CandidateService,
    public matDialog: MatDialog,
  ) {
  }

  public card: FullCandidateModel;

  public editForm = new FormGroup({
    name: new FormControl('', [Validators.required, customValidator()]),
    surname: new FormControl('', [Validators.required, customValidator()]),
    phone: new FormControl(''),
    email: new FormControl('', Validators.email),
    birthDate: new FormControl('', Validators.required),
  });

  public submit(): void {
    this.card = this.editForm.value;
    this.service.currentCandidate = this.data;
    this.service.currentFullCandidate = this.card;
    this.service.editCandidate();
  }
}


