import { FormGroup } from "@angular/forms";
import { CandidateModel } from "src/app/candidate/models/candidate.model";


export function setValueForEditForm(form: FormGroup, candidate: CandidateModel): FormGroup {
    form.setValue({
        fullname: candidate.fullname,
        phone: candidate.phone,
        email: candidate.email,
        birthDate: candidate.birthDate
    });
    return form;
}