import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, Form, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { CandidateModel, FullCandidateModel } from "../models/candidate.model";
import { CandidateService } from "../services/candidate.service";

@Component({
  selector: 'app-card-candidate-create',
  templateUrl: 'card-candidate-create.component.html',
  styleUrls: ['card-candidate-create.component.scss']
})
export class CandidateCardCreateComponent {

  
  public form: FormGroup;
  public candidateService = this.service;

  public card: FullCandidateModel;



  constructor(
    private service: CandidateService,
    private matDialog: MatDialog,
    public fb: FormBuilder
  ) { 
    this.form = fb.group({
      name: ['', [Validators.required, customValidator()]],
      surname: new FormControl('', [Validators.required, customValidator()]),
      phone: new FormControl(''),
      email: new FormControl('', Validators.email),
      birthDate: new FormControl('', Validators.required),
      originId: new FormControl(''),
      skills: this.fb.array([])
    })
  }


  public addSkill(): void {
    const add = this.form.controls['skills'] as FormArray;
    add.push(this.fb.group({
      skill: []
    }))
  }

  public deleteSkillGroup(index: number): void {
    const add = this.form.get('skills') as FormArray;
    add.removeAt(index)
  }

  public submit(): void {
    
    this.card = this.form.value;
    this.service.currentFullCandidate = this.card;
    console.log(this.service.currentFullCandidate);
    this.service.createCandidate();
  }
}


