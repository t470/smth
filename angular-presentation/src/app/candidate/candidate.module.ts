import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTooltipModule} from "@angular/material/tooltip";
import {CandidateComponent} from "./candidate.component";
import {CandidateRoutingModule} from "./candidate-routing.module";
import {CardCandidateComponent} from "./card-candidate/card-candidate.component";
import {CandidateService} from "./services/candidate.service";
import {RecruiterModule} from "../recruiter/recruiter.module";
import { CandidateCardEditComponent } from './card-candidate-edit/card-candidate-edit.component';
import { CandidateCardCreateComponent } from './card-candidate-create/card-candidate-create.component';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';



@NgModule({
  declarations: [
    CandidateComponent,
    CardCandidateComponent,
    CandidateCardEditComponent,
    CandidateCardCreateComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    FormsModule,
    MatTooltipModule,
    CandidateRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    RecruiterModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule
  ],
  providers: [
    CandidateService
  ]
})
export class CandidateModule { }
