import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CandidateModel, FullCandidateModel, GetCandidatesResponseModel } from "../models/candidate.model";
import { merge, Observable, Subject } from "rxjs";
import { GetRecruitersResponseModel, RecruiterModel } from "../../models/recruiter.model";
import { catchError, map, share, switchMap, takeUntil } from "rxjs/operators";
import { matError } from "src/assets/shared/func/error.function";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CatalogModel, GetCatalogResponceModel } from "src/app/models/catalog.model";

@Injectable()

export class CandidateService {

  public destroy$ = new Subject<boolean>();

  private createCandidateSubject = new Subject<boolean>();
  public createCandidateAction$ = this.createCandidateSubject.asObservable();
  private deleteCandidateSubject = new Subject<number>();
  public deleteCandidateAction$ = this.deleteCandidateSubject.asObservable();
  private editCandidateSubject = new Subject<number>();
  public editCandidateAction$ = this.editCandidateSubject.asObservable();
  
  public options = {
    headers: new HttpHeaders({
      'api-version': '1.0',
    })
  };


  public currentCandidate: CandidateModel;
  public currentFullCandidate: FullCandidateModel;

  public candidatesCards$ = this.getCandidates();
  public origins$ = this.getOrigins();
  public skills$ = this.getSkills();


  public candidatesWithActions$ = merge(
    this.candidatesCards$,
    this.createCandidateAction$.pipe(
      switchMap(() => this.createCandidateAction()),
      switchMap(() => this.getCandidates()),
    ),
    this.deleteCandidateAction$.pipe(
      switchMap(() => this.deleteCandidateAction()),
      switchMap(() => this.getCandidates()),
    ),
    this.editCandidateAction$.pipe(
      switchMap(() => this.putCandidateAction()),
      switchMap(() => this.getCandidates()),
    ),
  );

  public createCandidate(): void {
    this.createCandidateSubject.next();
  }

  public deleteCandidate(): void {
    this.deleteCandidateSubject.next();
  }

  public editCandidate(): void {
    this.editCandidateSubject.next();
  }



  constructor(private httpClient: HttpClient,private _snackBar: MatSnackBar) {
  }

  private getCandidates(): Observable<Array<CandidateModel>> {
    return this.httpClient.get<GetCandidatesResponseModel>
      ('http://dev0.devoxsoftware.com:4444/api/Candidate?pageNumber=1&pageSize=10', this.options).pipe(map(result => result.items),
        catchError((error: Error) => {
          matError(error, this._snackBar);
          throw new Error(error.message);
        }),
        share(),
        takeUntil(this.destroy$));
  }
  

  private putCandidateAction(): Observable<FullCandidateModel> {
    return this.httpClient.put<FullCandidateModel>
      (`http://dev0.devoxsoftware.com:4444/api/Candidate/${this.currentCandidate.id}`, this.currentFullCandidate, this.options).pipe(
        catchError((error: Error) => {
          matError(error, this._snackBar);
          throw new Error(error.message);
        }),
        takeUntil(this.destroy$));
  }

  private createCandidateAction() {
    let skillId = this.currentFullCandidate.skills.map(x => x['skill'].id);
    console.log(skillId);
    
    return this.httpClient.post<FullCandidateModel>('http://dev0.devoxsoftware.com:4444/api/Candidate', {
      name: this.currentFullCandidate.name,
      surname: this.currentFullCandidate.surname,
      email: this.currentFullCandidate.email,
      phone: this.currentFullCandidate.phone,
      birthDate: this.currentFullCandidate.birthDate,
      originId: this.currentFullCandidate.originId,
      skillsIds: skillId
    }, this.options).pipe(
      catchError((error: Error) => {
        matError(error, this._snackBar);
        throw new Error(error.message);
      })
    );
  }

  private getOrigins() : Observable<Array<CatalogModel>>
  {
    return this.httpClient.get<GetCatalogResponceModel>('http://dev0.devoxsoftware.com:4444/api/Origin?pageNumber=1&itemsPerPage=10', this.options).pipe(
      map(x => x.items),
      share(),
      takeUntil(this.destroy$)
    );
  }

  private getSkills() : Observable<Array<CatalogModel>>
  {
    return this.httpClient.get<GetCatalogResponceModel>('http://dev0.devoxsoftware.com:4444/api/Skill?pageNumber=1&itemsPerPage=10', this.options).pipe(
      map(x => x.items),
      share(),
      takeUntil(this.destroy$)
    );
  }

  private deleteCandidateAction(): Observable<GetCandidatesResponseModel> {
    return this.httpClient.delete<GetCandidatesResponseModel>
      (`http://dev0.devoxsoftware.com:4444/api/Candidate/${this.currentCandidate.id}`, this.options).pipe(
        catchError((error: Error) => {
          matError(error, this._snackBar);
          throw new Error(error.message);
        })
      );
  }

}
