export interface GetCandidatesResponseModel {
  items: Array<CandidateModel>;
}

export interface GetCandidateByIdResponseMOdel {
  id: number;
  name: string;
  surname: string;
  email: string;
  phone: string;
  birthDate: string;
  origin: Origin;
}

export interface CandidateModel {
  id: number;
  fullname: string;
  email: string;
  phone: string;
  birthDate: string;
  origin: Origin;
}

export interface Origin {
  title: string;
}

export interface FullCandidateModel {
  name: string;
  surname: string;
  email: string;
  phone: string;
  birthDate: string;
  originId: number;
  skills: Array<Skill>;
}

export interface Skill {
  id: number;
  title: string;
}

