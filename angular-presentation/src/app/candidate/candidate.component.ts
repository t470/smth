import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CandidateCardCreateComponent } from "./card-candidate-create/card-candidate-create.component";
import { CandidateCardEditComponent } from "./card-candidate-edit/card-candidate-edit.component";
import { CandidateService } from "./services/candidate.service";

@Component({
  selector: 'app-candidate',
  templateUrl: 'candidate.component.html',
  styleUrls: ['candidate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandidateComponent implements OnDestroy {

  constructor(private service: CandidateService,private matDialog: MatDialog) {
  }

  public serviceCandidate = this.service.candidatesWithActions$;

  public onOpenDialogClick(): void{
    this.matDialog.open(CandidateCardCreateComponent);
  }

  ngOnDestroy(): void {
    this.service.destroy$.next();
  }
}
