export interface GetCatalogResponceModel{
    items: Array<CatalogModel>;
}

export interface CatalogModel
{
    id: number;
    title: string;
}