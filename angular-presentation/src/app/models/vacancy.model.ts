
export interface GetVacancyResponseModel {
    items: Array<VacancyModel>;
}


export interface VacancyModel {
    id: number;
    title: string;
    date: string;
    maxSalary: string;
    jobType: JobType;
    description: string;
}
export interface PostVacancyResponseModel {
    title: string;
    date: string;
    maxSalary: number;
    jobTypeId: number;
    description: string;
    statusId: number;
    recruiterId: number;
    seniorityLevelId: number
}


export interface JobType {
    id: number;
    title: string;
}