export interface GetRecruitersResponseModel{
    items:Array<RecruiterModel>;
}

export interface RecruiterModel{
    id: number;
    name:string;
    surname:string;
    email:string;
    phone:string;
    birthDate: string;
}
