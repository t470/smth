import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { CardComponent } from '../card/card.component';
import { MainRoutingModule } from './main-routing.module';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    CardComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    TranslateModule
  ]
})
export class MainModule { }
