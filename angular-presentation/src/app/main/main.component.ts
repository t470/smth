import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { RecruiterModel, GetRecruitersResponseModel } from "../models/recruiter.model";
import { GetVacancyResponseModel } from "../models/vacancy.model";
import { GetDataService } from "./services/get-data.service";

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.scss']
})

export class MainComponent implements OnInit {

  public recruiterList: GetRecruitersResponseModel;
  public vacancyList: GetVacancyResponseModel;

  public recruiterItems;
  public vacancyItems;
  
  constructor(private service: GetDataService, private translate: TranslateService){ }

  ngOnInit(){
    this.getRecruiter();
  }

  public async getRecruiter(){
    this.recruiterList = await this.service.getEntity('Recruiter');
    this.recruiterItems = this.recruiterList.items;
    
    this.vacancyList = await this.service.getEntity('Vacancy');
    console.log(this.vacancyList);
    this.vacancyItems = this.vacancyList.items;
  }
}


