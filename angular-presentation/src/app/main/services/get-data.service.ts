import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {  GetRecruitersResponseModel } from "../../models/recruiter.model";
import { GetVacancyResponseModel } from "../../models/vacancy.model";

@Injectable({
    providedIn: 'root'
})

export class GetDataService {

    constructor(private httpClient: HttpClient)  { }

    getEntity(entity: string) {
        let options = {
            headers: new HttpHeaders({
                'accept': 'text/plain',
                'api-version': '1.0'
            })
        };

        let promise;

        if (entity === 'Recruiter') {
            promise = this.httpClient.get<GetRecruitersResponseModel>(`http://dev0.devoxsoftware.com:4444/api/${entity}?pageNumber=1&pageSize=10`, options).toPromise();
        }
        if(entity === 'Vacancy'){
            promise = this.httpClient.get<GetVacancyResponseModel>(`http://dev0.devoxsoftware.com:4444/api/${entity}?pageNumber=1&pageSize=10`, options).toPromise();
        }

        return promise; 
    }


}