import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './header/menu/header-menu.component';
import { HeaderUserInfoComponent } from './header/userInfo/header-user-info.component';
import { MatIconModule } from '@angular/material/icon';
import { RecruiterModule } from './recruiter/recruiter.module';
import { VacancyModule } from './vacancy/vacancy.module';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GetDataService } from './main/services/get-data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from "@angular/material/dialog";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';
import { CandidateModule } from "./candidate/candidate.module";
import { InterviewComponent } from './interview/interview.component';
import { InterviewModule } from './interview/interview.module';
import { InterviewsFacade } from './interview/store/interview.facade';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { MainModule } from './main/main.module';
import { AuthDirective } from './directive/auth.directive';
import { MatListModule } from '@angular/material/list';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    HeaderUserInfoComponent,
    AuthDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RecruiterModule,
    VacancyModule,
    MatIconModule,
    MainModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CandidateModule,
    InterviewModule,
    MatListModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    StoreModule.forRoot({}, {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),

    AuthModule.forRoot({
      domain: 'devoxsoftware.eu.auth0.com',
      clientId: 'hgOJjh1n44wEU200ltBzkrq20IOAL6Pf',
      audience: 'https://onboardingtracker/api',
      scope: 'read:current_user',
      httpInterceptor: {
        allowedList: [
          { uri: 'http://dev0.devoxsoftware.com:4444/api/Recruiter', },
          { uri: 'http://dev0.devoxsoftware.com:4444/api/Vacancy', },
          { uri: 'http://dev0.devoxsoftware.com:4444/api/Interview', },
          { uri: 'http://dev0.devoxsoftware.com:4444/api/*' },
        ]
      }
    })],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}