import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecruiterModel } from 'src/app/models/recruiter.model';
import { PostVacancyResponseModel, VacancyModel } from 'src/app/models/vacancy.model';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { setValueToForm } from '../func/set-value-to-form.function';
import { VacancyService } from '../services/vacancy.service';

@Component({
  selector: 'app-card-vacancy-create',
  templateUrl: 'card-vacancy-create.component.html',
  styleUrls: ['card-vacancy-create.component.scss']
})
export class CardVacancyCreate implements OnInit {

  constructor(
    private service: VacancyService
  ) { }

  public vacancyService = this.service;

  public card: PostVacancyResponseModel;

  public createForm = new FormGroup({
    title: new FormControl('', [Validators.required, customValidator()]),
    date: new FormControl('', [Validators.required, customValidator()]),
    maxSalary: new FormControl(''),
    statusId: new FormControl('', Validators.email),
    description: new FormControl('', Validators.required),
    recruiterId: new FormControl('', Validators.required),
    seniorityLevelId: new FormControl('', Validators.required),
    jobTypeId: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
    this.createForm = setValueToForm(this.createForm);
  }

  public submit(): void {
    this.card = this.createForm.value;
    this.service.currentVacancyForCreate = this.card;
    this.service.createVacancy();
  }
}


