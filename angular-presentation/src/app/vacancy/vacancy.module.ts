import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacancyRoutingModule } from './vacancy-routing.module';
import { VacancyComponent } from './vacancy.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CardVacancyComponent } from './card-vacancy/card-vacancy.component';
import { MatInputModule } from '@angular/material/input'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTooltipModule} from "@angular/material/tooltip";
import { CardVacancyCreate } from './card-vacancy-create/card-vacancy-create.component';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';



@NgModule({
  declarations: [
    VacancyComponent,
    CardVacancyComponent,
    CardVacancyCreate
  ],
  imports: [
    CommonModule,
    VacancyRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    FormsModule,
    MatTooltipModule,
    MatTableModule,
    MatSelectModule,
    MatFormFieldModule,
    ReactiveFormsModule
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule
  ]
  // ,
  // providers: [
  //   GetDataService
  // ]
})
export class VacancyModule { }
