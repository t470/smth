import { ChangeDetectionStrategy, Component, OnChanges, OnDestroy, OnInit, SimpleChange } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';
import { GetVacancyResponseModel, VacancyModel } from '../models/vacancy.model';
import { CardVacancyCreate } from './card-vacancy-create/card-vacancy-create.component';
import { VacancyService } from './services/vacancy.service';

@Component({
    selector: 'app-vacancy',
    templateUrl: './vacancy.component.html',
    styleUrls: ['./vacancy.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VacancyComponent implements OnDestroy {

    constructor(private service: VacancyService, private matDialog:MatDialog) { }

    public serviceVacancy = this.service.vacancyWithActions$;

    public onOpenDialogClick(): void{
      this.matDialog.open(CardVacancyCreate);
    }
  
    ngOnDestroy(): void {
      this.service.destroy$.next();
    }
}
