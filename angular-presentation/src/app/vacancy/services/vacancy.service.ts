import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { merge, Observable, Subject } from "rxjs";
import { catchError, map, share, switchMap, takeUntil } from "rxjs/operators";
import { CatalogModel, GetCatalogResponceModel } from "src/app/models/catalog.model";
import { GetRecruitersResponseModel, RecruiterModel } from "src/app/models/recruiter.model";
import { GetVacancyResponseModel, PostVacancyResponseModel, VacancyModel } from "src/app/models/vacancy.model";
import { matError } from "src/assets/shared/func/error.function";

@Injectable({
    providedIn: 'root'
})

export class VacancyService {


    public options = {
        headers: new HttpHeaders({
            'accept': 'text/plain',
            'api-version': '1.0'
        })
    };

    destroy$ = new Subject<boolean>();

    private createVacancySubject = new Subject<boolean>();
    public createVacancyAction$ = this.createVacancySubject.asObservable();
    private deleteVacancySubject = new Subject<number>();
    public deleteVacancyAction$ = this.deleteVacancySubject.asObservable();
    private editVacancySubject = new Subject<VacancyModel>();
    public editVacancyAction$ = this.editVacancySubject.asObservable();
    
    public currentVacancy: VacancyModel;
    public currentVacancyForCreate: PostVacancyResponseModel;


    public vacancyCards$ = this.getVacancies();
    public jobTypes$ = this.getJobTypes();
    public recruiters$ = this.getRecruiters();
    public statuses$ = this.getStatuses();
    public seniorityLevels$ = this.getSeniorityLevels();

    public vacancyWithActions$ = merge(
        this.vacancyCards$,
        this.createVacancyAction$.pipe(
            switchMap(() => this.createVacancyAction()),
            switchMap(() => this.getVacancies()),
        ),
        this.deleteVacancyAction$.pipe(
            switchMap(() => this.deleteVacancyAction()),
            switchMap(() => this.getVacancies()),
        ),
        this.editVacancyAction$.pipe(
            switchMap(() => this.putVacancyAction()),
            switchMap(() => this.getVacancies()),
        ),
    );

    public createVacancy(): void {
        this.createVacancySubject.next();
    }

    public deleteVacancy(): void {
        this.deleteVacancySubject.next();
    }

    public editVacancy(): void {
        this.editVacancySubject.next();
    }



    constructor(private httpClient: HttpClient, public _snakBar: MatSnackBar) { }

    private getVacancies(): Observable<Array<VacancyModel>> {

        return this.httpClient.get<GetVacancyResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Vacancy?pageNumber=1&pageSize=10`, this.options)
            .pipe(map(x => (
                x.items
            )
            ),
                catchError((error: Error) => {
                    matError(error, this._snakBar);
                    throw new Error(error.message);
                }),
                share(),
                takeUntil(this.destroy$)
            );

    }


    private createVacancyAction(): Observable<VacancyModel> {

        return this.httpClient.post<VacancyModel>(`http://dev0.devoxsoftware.com:4444/api/Vacancy/`,{
            title: this.currentVacancyForCreate.title,
            date: this.currentVacancyForCreate.date,
            maxSalary: this.currentVacancyForCreate.maxSalary,
            description: this.currentVacancyForCreate.description,
            recruiterId: this.currentVacancyForCreate.recruiterId,
            statusId: this.currentVacancyForCreate.statusId,
            seniorityLevelId: this.currentVacancyForCreate.seniorityLevelId,
            jobTypeId: this.currentVacancyForCreate.jobTypeId
        }, this.options)
            .pipe(
                catchError((error: Error) => {
                    matError(error, this._snakBar);
                    throw new Error(error.message);
                }),
                takeUntil(this.destroy$));
    }


    private getRecruiters(): Observable<Array<RecruiterModel>> {
        return this.httpClient.get<GetRecruitersResponseModel>('http://dev0.devoxsoftware.com:4444/api/Recruiter', this.options).pipe(
            map(x => x.items),
            catchError((error: Error) => {
                matError(error, this._snakBar);
                throw new Error(error.message);
            }),
            takeUntil(this.destroy$)
        );
    }

    private putVacancyAction(): Observable<VacancyModel> {

        return this.httpClient.put<VacancyModel>(`http://dev0.devoxsoftware.com:4444/api/Vacancy/${this.currentVacancy.id}`, {
            title: this.currentVacancy.title,
            maxSalary: parseFloat(this.currentVacancy.maxSalary),
            description: this.currentVacancy.description,
        }, this.options)
            .pipe(
                catchError((error: Error) => {
                    matError(error, this._snakBar);
                    throw new Error(error.message);
                }),
                takeUntil(this.destroy$));
    }

    private getJobTypes() : Observable<Array<CatalogModel>>
    {
      return this.httpClient.get<GetCatalogResponceModel>('http://dev0.devoxsoftware.com:4444/api/JobType?pageNumber=1&itemsPerPage=10', this.options).pipe(
        map(x => x.items),
        share(),
        takeUntil(this.destroy$)
      );
    }
    
    
    private getSeniorityLevels() : Observable<Array<CatalogModel>>
    {
      return this.httpClient.get<GetCatalogResponceModel>('http://dev0.devoxsoftware.com:4444/api/SeniorityLevel?pageNumber=1&itemsPerPage=10', this.options).pipe(
        map(x => x.items),
        share(),
        takeUntil(this.destroy$)
      );
    }
    
    private getStatuses() : Observable<Array<CatalogModel>>
    {
      return this.httpClient.get<GetCatalogResponceModel>('http://dev0.devoxsoftware.com:4444/api/Status?pageNumber=1&itemsPerPage=10', this.options).pipe(
        map(x => x.items),
        share(),
        takeUntil(this.destroy$)
      );
    }

    private deleteVacancyAction(): Observable<VacancyModel> {
        return this.httpClient.delete<VacancyModel>(`http://dev0.devoxsoftware.com:4444/api/Vacancy/${this.currentVacancy.id}`, this.options)
            .pipe(
                catchError((error: Error) => {
                    matError(error, this._snakBar);
                    throw new Error(error.message);
                }),
                takeUntil(this.destroy$));
    }


}