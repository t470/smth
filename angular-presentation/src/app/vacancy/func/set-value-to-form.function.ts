import { FormGroup } from "@angular/forms";


export function setValueToForm(form: FormGroup): FormGroup {
    form.setValue({
        title: 'Smth',
        date: '02/02/2000',
        maxSalary: 2000,
        statusId: 1,
        description: 'Some desc',
        recruiterId: 1,
        seniorityLevelId: 1,
        jobTypeId:1,
    })
    return form;
}