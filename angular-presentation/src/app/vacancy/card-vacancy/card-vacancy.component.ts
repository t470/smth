import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Input, OnInit, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { VacancyModel } from 'src/app/models/vacancy.model';
import { VacancyService } from '../services/vacancy.service';

@Component({
  selector: 'app-card-vacancy',
  templateUrl: './card-vacancy.component.html',
  styleUrls: ['./card-vacancy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardVacancyComponent {

  @Input() card: VacancyModel;

  public isDeleted: boolean = false;

  constructor(private service: VacancyService) { }

  public vacancyService = this.service;

  public isEdit: Boolean = false;


  public edit(): void {
    this.isEdit = !this.isEdit;
  }

  public submit(): void {
    this.service.currentVacancy = this.card;
    this.service.editVacancy();
  }

  public delete(): void {
    this.service.currentVacancy = this.card;
    this.service.deleteVacancy();
  }




}
