import { Directive, ElementRef, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[auth]'
})
export class AuthDirective {


    @Input('auth')
    public set tryAuth(v: boolean) {
        this.smth = v;
        if (this.smth === false)
            this.el.nativeElement.style.display = 'none';
        else {
            this.el.nativeElement.style.display = 'block';
        }

    }

    private smth;

    constructor(private el: ElementRef) { }

}
