import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit{
  
  constructor(private el: ElementRef) { }

  @Input('appHighlight') highlightColor = '';
  ngOnInit() {
    this.el.nativeElement.style.backgroundColor = this.highlightColor;
  }

}