import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appShadow]'
})
export class ShadowDirective {

    constructor(private el: ElementRef) { }

    @Input() defaultColor = '';
    
    public shadow = '10px 5px 5px';
  
    @Input('appShadow') highlightColor = '';
  
    @HostListener('mouseenter') onMouseEnter() {
      this.highlight(this.highlightColor || this.defaultColor, this.shadow);
    }

    @HostListener('mouseleave') onMouseLeave() {
      this.highlight('','none');
    }
  
    private highlight(color: string, shadow:string) {
      this.el.nativeElement.style.boxShadow = `${shadow} ${color}`; 
    }
}