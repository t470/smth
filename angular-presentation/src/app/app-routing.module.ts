import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecruiterModule } from './recruiter/recruiter.module';
import { VacancyModule } from './vacancy/vacancy.module';
import { CandidateModule } from "./candidate/candidate.module";
import { InterviewModule } from './interview/interview.module';
import { MainModule } from './main/main.module';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthGuard } from '@auth0/auth0-angular';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => MainModule,
    data: {
      breadcrumb: 'Home'
    }
  },
  {
    path: 'vacancy',
    loadChildren: () => VacancyModule,
    canActivate: [AuthGuardService],
    data: {
      breadcrumb: 'Vacancy'
    }
  },
  {
    path: 'recruiter',
    loadChildren: () => RecruiterModule,
    canActivate: [AuthGuardService],
    data: {
      breadcrumb: 'Recruiter'
    }
  },
  {
    path: 'candidate',
    loadChildren: () => CandidateModule,
    canActivate: [AuthGuardService],
    data: {
      breadcrumb: 'Candidate'
    }
  },
  {
    path: 'interview',
    loadChildren: () => InterviewModule,
    canActivate: [AuthGuardService],
    data: {
      breadcrumb: 'Interview'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
