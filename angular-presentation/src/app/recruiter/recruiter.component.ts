import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { GetRecruitersResponseModel, RecruiterModel } from '../models/recruiter.model';
import { RecruiterService } from './services/recruiter.service';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators'
import { MatSnackBar, MatSnackBarDismiss } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { CardRecruiterCreateComponent } from './card-recruiter-create/card-recruiter-create.component';

@Component({
  selector: 'app-recruiter',
  templateUrl: './recruiter.component.html',
  styleUrls: ['./recruiter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecruiterComponent implements OnDestroy {



  constructor(public service: RecruiterService,public matDialog: MatDialog) {
  }

  public serviceRecruiter = this.service.recruitersWithActions$;

  public onOpenDialogClick(): void{
    this.matDialog.open(CardRecruiterCreateComponent);
  }

  ngOnDestroy(): void {
    this.service.destroy$.next();
  }

}
