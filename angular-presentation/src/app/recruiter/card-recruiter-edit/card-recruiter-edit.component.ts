import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { setValueToEditForm } from 'src/app/recruiter/func/set-value-to-editForm.function';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { GetRecruitersResponseModel, RecruiterModel } from "../../models/recruiter.model";
import { CardRecruiterComponent } from '../card-recruiter/card-recruiter.component';
import { mapFromFormToRecruiter } from '../func/map-from-form-to-recruiter.function';
import { RecruiterService } from '../services/recruiter.service';

@Component({
  selector: 'app-card-edit',
  templateUrl: 'card-recruiter-edit.component.html',
  styleUrls: ['card-recruiter-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardRecruiterEditComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: RecruiterModel,
    private service: RecruiterService
  ) {
  }

  public card: RecruiterModel;

  public editForm = new FormGroup({
    name: new FormControl('', [Validators.required, customValidator()]),
    surname: new FormControl('', Validators.required),
    phone: new FormControl(''),
    email: new FormControl('', Validators.email),
    birthDate: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
    console.log(this.data);
    this.card = this.data;

    this.editForm = setValueToEditForm(this.editForm, this.card)

  }

  public submit(): void {
    this.card = mapFromFormToRecruiter(this.card, this.editForm);
    this.service.currentRecruiter = this.card;
    this.service.editRecruiter();
  }
}

