import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecruiterRoutingModule } from './recruiter-routing.module';
import { RecruiterComponent } from './recruiter.component';
import { GetDataService } from '../main/services/get-data.service';
import {CardRecruiterComponent} from "./card-recruiter/card-recruiter.component";
import {MatCardModule} from "@angular/material/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import { BirthDatePipe } from 'src/assets/shared/pipe/birthday.pipe';
import { RecruiterService } from './services/recruiter.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CardRecruiterEditComponent } from './card-recruiter-edit/card-recruiter-edit.component';
import { CardRecruiterCreateComponent } from './card-recruiter-create/card-recruiter-create.component';


@NgModule({
  declarations: [
    RecruiterComponent,
    CardRecruiterComponent,
    BirthDatePipe,
    CardRecruiterEditComponent,
    CardRecruiterCreateComponent,
    
  ],
  imports: [
    CommonModule,
    RecruiterRoutingModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  exports: [
    BirthDatePipe
  ],
  providers: [
    RecruiterService
  ]
})
export class RecruiterModule { }
