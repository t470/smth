import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BehaviorSubject, merge, Observable, Subject } from "rxjs";
import { catchError, map, switchMap, takeUntil } from "rxjs/operators";
import { GetRecruitersResponseModel, RecruiterModel } from "src/app/models/recruiter.model";
import { matError } from "src/assets/shared/func/error.function";
@Injectable()

export class RecruiterService {

    public options = {
        headers: new HttpHeaders({
            'accept': 'text/plain',
            'api-version': '1.0',
        })
    };

    public destroy$ = new Subject<boolean>();

    private createRecruiterSubject = new Subject<boolean>();
    public createRecruiterAction$ = this.createRecruiterSubject.asObservable();
    private deleteRecruiterSubject = new Subject<number>();
    public deleteRecruiterAction$ = this.deleteRecruiterSubject.asObservable();
    private editRecruiterSubject = new Subject<RecruiterModel>();
    public editRecruiterAction$ = this.editRecruiterSubject.asObservable();


    public currentRecruiter: RecruiterModel;

    public recruitersCards$ = this.getRecruiters();

    public recruitersWithActions$ = merge(
        this.recruitersCards$,
        this.createRecruiterAction$.pipe(
            switchMap(() => this.createRecruiterAction()),
            switchMap(() => this.getRecruiters()),
        ),
        this.deleteRecruiterAction$.pipe(
            switchMap(() => this.deleteRecruiterAction()),
            switchMap(() => this.getRecruiters()),
        ),
        this.editRecruiterAction$.pipe(
            switchMap(() => this.putRecruiterAction()),
            switchMap(() => this.getRecruiters()),
        ),
    );

    public createRecruiter(): void {
        this.createRecruiterSubject.next();
    }

    public deleteRecruiter(): void {
        this.deleteRecruiterSubject.next();
    }

    public editRecruiter(): void {
        this.editRecruiterSubject.next();
    }

    constructor(private httpClient: HttpClient, private _snackBar: MatSnackBar) { }

    private getRecruiters(): Observable<Array<RecruiterModel>> {
        return this.httpClient.get<GetRecruitersResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Recruiter?pageNumber=1&pageSize=10`, this.options)
            .pipe(map(x => (
                x.items
            )),

                catchError((error: Error) => {
                    matError(error, this._snackBar);
                    throw new Error(error.message);
                }),
                takeUntil(this.destroy$)
            );

    }

    private createRecruiterAction(): Observable<RecruiterModel> {
        return this.httpClient.post<RecruiterModel>('http://dev0.devoxsoftware.com:4444/api/Recruiter', this.currentRecruiter, this.options).pipe(
            catchError((error: Error) => {
                matError(error, this._snackBar);
                throw new Error(error.message);
            }),
            takeUntil(this.destroy$)
        );
    }

    private putRecruiterAction(): Observable<GetRecruitersResponseModel> {
        return this.httpClient.put<GetRecruitersResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Recruiter`, this.currentRecruiter, this.options).pipe(
            catchError((error: Error) => {
                matError(error, this._snackBar);
                throw new Error(error.message);
            }),
            takeUntil(this.destroy$)
        )
    }

    private deleteRecruiterAction(): Observable<GetRecruitersResponseModel> {
        return this.httpClient.delete<GetRecruitersResponseModel>(`http://dev0.devoxsoftware.com:4444/api/Recruiter/${this.currentRecruiter.id}`, this.options).pipe(
            catchError((error: Error) => {
                matError(error, this._snackBar);
                throw new Error(error.message);
            }),
            takeUntil(this.destroy$)
        )
    }

}