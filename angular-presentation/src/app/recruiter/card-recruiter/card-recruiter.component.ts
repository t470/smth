import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { RecruiterModel } from "../../models/recruiter.model";
import { MatDialog } from "@angular/material/dialog";
import { RecruiterService } from '../services/recruiter.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardRecruiterEditComponent } from "../card-recruiter-edit/card-recruiter-edit.component";
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-card-recruiter',
  templateUrl: './card-recruiter.component.html',
  styleUrls: ['./card-recruiter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardRecruiterComponent {

  @Input() card: RecruiterModel;

  public isDeleted: boolean = false;

  constructor(private matDialog: MatDialog, private service: RecruiterService, private _snackBar: MatSnackBar) {
  }

  public onOpenDialogClick(): void {
    console.log(this.card);
    this.matDialog.open(CardRecruiterEditComponent, {
      data: this.card,
    });
  }

  public delete(): void {
    this.service.currentRecruiter = this.card;
    this.service.deleteRecruiter();
  }

  public openSnackBar(message: string): void {
    this._snackBar.open(message, "Close").afterDismissed();
    console.log(message);
  }
}
