import { FormGroup } from "@angular/forms";
import { RecruiterModel } from "src/app/models/recruiter.model";


export function setValueToEditForm(form: FormGroup, recruiter: RecruiterModel): FormGroup {
    form.setValue({
        name: recruiter.name,
        surname: recruiter.surname,
        phone: recruiter.phone,
        email: recruiter.email,
        birthDate: recruiter.birthDate
    });

    return form;
}