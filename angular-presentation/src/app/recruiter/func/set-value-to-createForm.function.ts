import { FormGroup } from "@angular/forms";


export function setValueToCreateForm(form: FormGroup): FormGroup {
    form.setValue({
        name: 'FirstName',
        surname: 'SecondName',
        phone: '+380 65 123-12-12',
        email: 'someEmail@gmail.com',
        birthDate: '02/02/2000',
    });
    return form;
}