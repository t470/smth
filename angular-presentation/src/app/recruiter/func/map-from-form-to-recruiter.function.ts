import { FormGroup } from "@angular/forms";
import { RecruiterModel } from "src/app/models/recruiter.model";

export function mapFromFormToRecruiter(recruiter: RecruiterModel, form: FormGroup): RecruiterModel{
    recruiter.name = form.value.name;
    recruiter.surname = form.value.surname;
    recruiter.email = form.value.email;
    recruiter.phone = form.value.phone;
    recruiter.birthDate = form.value.birthDate;

    return recruiter;
}