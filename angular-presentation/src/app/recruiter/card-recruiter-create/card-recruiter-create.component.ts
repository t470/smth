import { Component, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecruiterModel } from 'src/app/models/recruiter.model';
import { customValidator } from 'src/assets/shared/func/validator.function';
import { setValueToCreateForm } from '../func/set-value-to-createForm.function';
import { RecruiterService } from '../services/recruiter.service';

@Component({
  selector: 'app-card-recruiter-create',
  templateUrl: 'card-recruiter-create.component.html',
  styleUrls: ['card-recruiter-create.component.scss']
})
export class CardRecruiterCreateComponent implements OnInit {

  constructor(
    private service: RecruiterService
  ) { }

  public card: RecruiterModel;

  public createForm = new FormGroup({
    name: new FormControl('', [Validators.required, customValidator()]),
    surname: new FormControl('', [Validators.required, customValidator()]),
    phone: new FormControl(''),
    email: new FormControl('', Validators.email),
    birthDate: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    this.createForm = setValueToCreateForm(this.createForm);
  }

  public submit(): void {
    this.card = this.createForm.value;
    this.service.currentRecruiter = this.card;
    this.service.createRecruiter();
  }
}


