import { Component, Input, OnInit } from '@angular/core';
import { RecruiterModel } from '../models/recruiter.model';
import { VacancyModel } from '../models/vacancy.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() set inputList(val: RecruiterModel | VacancyModel){
    if((val as RecruiterModel).name){
      this.recruiter = val as RecruiterModel;
      this.isRecruiter = true;
      return;
    }
      this.vacancy = val as VacancyModel;
      this.isVacancy = true;
  }

  public recruiter: RecruiterModel;
  public vacancy: VacancyModel;
  public isRecruiter = false;
  public isVacancy = false;

}


