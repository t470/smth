import { Injectable, OnInit } from '@angular/core';
import { Router, CanActivate, CanLoad } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) { }
  public permission: boolean;

  canActivate(): Observable<boolean> {
    return this.auth.isAuthenticated$;
  }

}