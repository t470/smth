import { Component, HostBinding, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-menu',
    templateUrl: './header-menu.component.html',
    styleUrls: ['./header-menu.component.scss']
})
export class MenuComponent implements OnInit {

    public authIsAuth: Observable<boolean>;

    constructor(private auth: AuthService) {

    }

    @HostBinding('class.navbar-opened') navbarOpened = false;

    ngOnInit(): void{
        this.authIsAuth = this.auth.isAuthenticated$;
    }

    public toggleNavbar(): void {
        this.navbarOpened = !this.navbarOpened;
    }
}
