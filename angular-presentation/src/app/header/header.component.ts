import { DOCUMENT } from '@angular/common';
import { SafeMethodCall } from '@angular/compiler';
import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Observable } from 'rxjs';
import { Breadcrumb } from './model/breadcrumb.model';
import { BreadcrumbService } from './service/breadcrumb.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


    public breadcrumbs$: Observable<Breadcrumb[]>;

    public authIsAuth: Observable<boolean>;

    public authLogIn: Observable<void>;

    public authLogOut: Observable<void>;

    constructor(@Inject(DOCUMENT) public document: Document, public auth: AuthService, private breadcrumbService: BreadcrumbService) {
        this.breadcrumbs$ = breadcrumbService.breadcrumbs$;
    }

    ngOnInit(): void {
        this.authIsAuth = this.auth.isAuthenticated$;
    }

    public login(): void {
        this.auth.loginWithRedirect();
    }

    public logout(): void {
        this.auth.logout({ returnTo: document.location.origin })
    }
}
