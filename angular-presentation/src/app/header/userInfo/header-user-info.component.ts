import { SafeMethodCall } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-user-info',
    templateUrl: './header-user-info.component.html',
    styleUrls: ['./header-user-info.component.scss']
})
export class HeaderUserInfoComponent {

    public country = {
        lang: 'en',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg/200px-Flag_of_Great_Britain_%281707%E2%80%931800%29.svg.png'
    };

    constructor(public auth: AuthService,
        private translate: TranslateService) { }

    public switch(): void {

        if (this.country.lang == 'en') {
            this.country.lang = 'ua';
            this.country.img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/250px-Flag_of_Ukraine.svg.png'
            this.translate.use(this.country.lang);
            return;
        }

        this.country.lang = 'en';
        this.country.img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg/200px-Flag_of_Great_Britain_%281707%E2%80%931800%29.svg.png';
        this.translate.use(this.country.lang);

    }
}
