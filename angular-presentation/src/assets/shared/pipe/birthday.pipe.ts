import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'birthday' })
export class BirthDatePipe implements PipeTransform {
    transform(date: string) {
        return date.replace(/^(\d+)\/(\d+)\/(\d+)$/, `$3-$2-$1`);
    }
}