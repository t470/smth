import { MatSnackBar } from "@angular/material/snack-bar";

export function matError(error: Error, _snackBar: MatSnackBar): void{

    console.log(error);
    _snackBar.open(error.message, "Close").afterDismissed();

}