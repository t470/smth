import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function customValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
  
      const value = control.value;
  
      if (!value) {
        return null;
      }
  
      let nameValid = /[^A-Z a-z0-9]+/.test(value);
  
      return nameValid ? { nameValid: true } : null;
  
    }
  }